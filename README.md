Cura LulzBot Edition
====

This repository has been retired in favor of [the original Cura LE repository](https://gitlab.com/lulzbot3d/cura-le/cura-lulzbot). Please submit any issues or bug reports there, as this one is no longer being maintained.
----------------

Cura LulzBot Edition is a re-work of [Cura 3.6 by Ultimaker](https://github.com/Ultimaker/Cura/tree/3.6) with modified tooling code for ease of use with LulzBot brand printers.

License
----------------
Cura and Cura LE are released under the terms of the LGPLv3 or higher. A copy of this license should be included with the software.
